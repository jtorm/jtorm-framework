# jTorm Title Method

Sets title of HTML document.

## Install
```js
npm install @jtorm/title-method
```


## Properties

| Option | Type       | Required | Description |
|--------|------------|----------|-------------|
| `t`    | `string`   | `true`   | Title.      |


## Example

```js
->title {
  t: 'Page';
}
```
