/*! (c) jTorm and other contributors | www.jtorm.com/license */
'use strict';

module.exports = {
    jTormMediaqueryMethod: {
        // DI
        // windowModel

        alias: 'mq',
        m: null,
        params: [
            'q'// Query
        ],

        init: function() {
            let w = this.windowModel;
            this.m = w.matchMedia || w.msMatchMedia;
        },

        validate: function (v) {
            return !!v.d.q;
        },

        handle: function (v) {
            v.io = {c: this.process(v.d.q)};
        },

        process: function (q) {
            if (this.m) {
                let r = this.m(q);
                return (r && r.matches);
            }

            return false;
        }
    }
};
