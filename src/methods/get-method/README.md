# jTorm Get Method

## Install
```js
npm install @jtorm/get-method
```

## Properties

| Option | Type     | Required | Description         |
|--------|----------|----------|---------------------|
| `h`    | `string` | `false`  | HTML template path. |
| `t`    | `string` | `false`  | TSS path.           |
| `d`    | `string` | `false`  | Data path.          |


## Example

```js
->get {
    t: 'path/to/global.tss';
}
```
