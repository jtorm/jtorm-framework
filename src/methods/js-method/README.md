# jTorm JS Method

Adds external JS to the plugin collection to process.

## Install
```js
npm install @jtorm/js-method
```


## Properties

| Option | Required | Type    |
|--------|----------|---------|
| `src`  | `true`   | string  |


## Example

```js
->js {
    href: 'https://cdn.example.com/test.js';
}
```
