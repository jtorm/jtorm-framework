# jTorm Language Model

Get text in current language.

## Install
```js
npm install @jtorm/language-model
```


## How To

get: sentence

set: language, sentence, translation

getDate: dateObj
