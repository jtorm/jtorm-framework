# jTorm Handler Wrapper

A wrapper for the main handler which adds events and prepares an isolated iteration.

## Install

```js
npm install @jtorm/handler-wrapper
```
