# jTorm Error Handler

This error handler prints the error with the viewmodel data to the console.

## Install

```js
npm install @jtorm/error-handler
```
