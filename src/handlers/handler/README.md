# jTorm Handler

Contains a handler for processing data, TSS and HTML.

## Install

```js
npm install @jtorm/handler
```
